# offilemessaging

## Project setup
```
npm install
```

```
In order to use API, run this commands: npm i json-server , npm run backend-data
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
