const $helpers = {
    prettyDate: (timestamp, justMoment = false) => {
        let date = new Date(timestamp)
        let today = new Date()
        let day = date.getDate()
        let month = date.getMonth()
        let year = date.getFullYear()
        let hours = date.getHours()
        let minutes = date.getMinutes()
        let ampm = hours >= 12 ? 'PM' : 'AM'
        let moment = today.getTime() - (24 * 60 * 60 * 1000)
        hours = hours % 12
        hours = hours ? hours : 12
        minutes = minutes < 10 ? '0'+ minutes : minutes
        if(moment > timestamp && justMoment == false) {
            return day + '/' + month + '/' + year
        } else {
            return hours + ':' + minutes + ' ' + ampm
        }
    }
}

export default $helpers