import axios from 'axios'

const axiosInstance = axios.create({
    baseURL: 'api'
})

// Add a response interceptor
axiosInstance.interceptors.response.use(
  function(response) {
    return response
  },
  function(error) {
    return Promise.reject(error)
  }
)

const getContactList = () => axiosInstance.get('contacts')
const getMessages = (id) => axiosInstance.get(id)
const pushMessage = (id, message) => axiosInstance.put(id, message)

export { getContactList, getMessages, pushMessage }