import Vue from 'vue'
import Vuex from 'vuex'
import { getContactList } from '../utils/api'
import * as _ from 'lodash'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    contacts: {}
  },
  getters: {},
  mutations: {
    SET_CONTACTS: (state, payload) => {
      _.map(payload, (v, k) => {
        Vue.set(state.contacts, k, v)
      })
    }
  },
  actions: {
    initContactList({ commit }) {
      getContactList().then((res) => {
        commit('SET_CONTACTS', res.data)
      })
    }
  }
})