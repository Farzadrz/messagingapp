import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';

Vue.use(Vuetify);

export default new Vuetify({
    theme: {
        dark: false,
        default: 'light',
        options: {
            customProperties: true
        },
        themes: {
            light: {
                primary: '#009688',
                secondary: '#272e31',
                borderGray: '#e5e5e5',
                background: '#fff',
                white: '#fff',
                stampColor: '#0009'
            },
            dark: {
                primary: '#009688',
                secondary: '#272e31',
                borderGray: '#555',
                background: '#333',
                white: '#fff',
                stampColor: '#fff'
            },
        }
    }
});